<?php

namespace PlanetaDelEste\Widgets\Behaviors;

use Backend\Classes\BackendController;
use Backend\Classes\ControllerBehavior;
use Flash;
use Lang;

/**
 * Class TrashController
 *
 * @package PlanetaDelEste\Widgets\Behaviors
 * @property \Backend\Classes\Controller|\Backend\Behaviors\ListController|\Backend\Behaviors\FormController controller
 */
class TrashController extends ControllerBehavior
{
    protected $action = null;

    public function __construct($controller)
    {
        parent::__construct($controller);

        $this->action = BackendController::$action;
    }

    /**
     * @param \October\Rain\Database\Builder $query
     * @param null|string                    $definition
     */
    public function listExtendQuery($query, $definition = null)
    {
        switch ($this->action) {
            case 'trashed':
                $query->onlyTrashed();
                break;
        }
    }

    public function formExtendQuery($query)
    {
        $query->withTrashed();
    }

    public function trashed()
    {
        $this->controller->pageTitle = $this->controller->pageTitle ?: Lang::get(
            $this->getConfig(
                'trashed[title]',
                'planetadeleste.widgets::lang.trashed.title'
            )
        );
        $this->controller->bodyClass = 'slim-container';
        $this->controller->makeLists();
    }

    public function trashed_onRestore()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {
            $listConfig = $this->controller->listGetConfig();

            /*
             * Create the model
             */
            $class = $listConfig->modelClass;
            /** @var \October\Rain\Database\Model $model */
            $model = new $class;

            $query = $model->newQuery();
            $query->onlyTrashed()->whereIn($model->getKeyName(), $checkedIds);

            /*
             * Restore records
             */
            $records = $query->get();

            if ($records->count()) {
                foreach ($records as $record) {
                    $record->restore();
                }

                Flash::success(Lang::get('backend::lang.list.restore_selected_success'));
            } else {
                Flash::error(Lang::get('backend::lang.list.restore_selected_empty'));
            }
        }

        return $this->controller->listRefresh();
    }

    public function trashed_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {
            $listConfig = $this->controller->listGetConfig();

            /*
             * Create the model
             */
            $class = $listConfig->modelClass;
            /** @var \October\Rain\Database\Model $model */
            $model = new $class;

            $query = $model->newQuery();
            $query->onlyTrashed()->whereIn($model->getKeyName(), $checkedIds);

            /*
             * Delete records
             */
            $records = $query->get();

            if ($records->count()) {
                foreach ($records as $record) {
                    $record->forceDelete();
                }

                Flash::success(Lang::get('backend::lang.list.delete_selected_success'));
            } else {
                Flash::error(Lang::get('backend::lang.list.delete_selected_empty'));
            }
        }

        return $this->controller->listRefresh();
    }
}